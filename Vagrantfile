# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  # https://docs.vagrantup.com.
  config.vm.box = "ubuntu/trusty64"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network "forwarded_port", guest: 5000, host: 5000

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false
	
    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end
  
  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  config.vm.provision "file", source: "./my.ini", destination: "/home/vagrant/my.ini"
  config.vm.provision "file", source: "./kallithea.conf", destination: "/home/vagrant/kallithea.conf"

  config.vm.provision :shell, :run => "always" do |shell|
    shell.inline = %{
    	sudo apt-get update

	# Install python and virtualenv
	sudo apt-get install python-pip=1.5.4-1ubuntu4 -y
	sudo apt-get install python-virtualenv=1.11.4-1ubuntu1 -y
	sudo apt-get install python-dev=2.7.5-5ubuntu3 -y
	sudo apt-get install git=1:1.9.1-1ubuntu0.3 -y
	
	sudo mkdir -p /srv/kallithea/venv
	sudo virtualenv /srv/kallithea/venv

	source /srv/kallithea/venv/bin/activate

	cd /srv/kallithea
	sudo mkdir repos
	sudo pip install kallithea
	
	cd /srv/kallithea/venv
	sudo mv /home/vagrant/my.ini .
	
	sudo paster setup-db --force-yes --user=admin --email=kallithea@centralengland.coop --password=password --repos=/srv/kallithea/repos my.ini

	sudo mkdir /srv/kallithea/log
	sudo mv /home/vagrant/kallithea.conf /etc/init/ 
	
	sudo start kallithea
    }
  end
end
