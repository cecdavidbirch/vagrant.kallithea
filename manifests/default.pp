# execute 'apt-get update'
exec { 'apt-update':                    # exec resource named 'apt-update'
  command => '/usr/bin/apt-get update'  # command this resource will run
}

# execute 'apt-install-puppet'
#exec { 'apt-install puppet':                    # exec resource named 'apt-update'
#  command => '/usr/bin/apt-get install puppet'  # 2.7.11 command this resource will run
#}

class { 'kallithea':
  ldap_support => true,
  manage_git => true,
  manage_python => true,
  port => 80,
  seed_db => true,
  service_enable => true,
  service_ensure => true,
  service_provider => init,
  whoosh_cronjob => true,
  config_hash => {
    "server:main" => {
      'host' => '0.0.0.0',
      'port' => '5000',
    }
  }
}